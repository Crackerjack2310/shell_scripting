#!/bin/bash
#https://bartsimons.me/sync-folders-and-files-on-linux-with-rsync-and-inotify/
# Supposed to run on rsync-host01, 
# change rsync-host02 to rsync-host01 to make a script that is meant to run on rsync-host02.

while false; do
	inotifywait -r -e modify,attrib,close_write,move,create,delete /home/dexterous/stuff/git/home/
	#rsync -az -e "ssh StrictHostKeyChecking=no"  /opt/syncfiles/ root@rsync-host02:/opt/syncfiles/
	echo "got an event"
	#rsync -az --delete /home/dexterous/stuff/git/home/ /home/dexterous/stuff/git/office
	#rsync -az -e "ssh -o StrictHostKeyChecking=no" /home/dexterous/stuff/git/home/ dexterous@localhost:/home/dexterous/stuff/git/office
	rsync -avz --delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /home/dexterous/stuff/git/home/ root@localhost:/home/dexterous/stuff/git/office/
done
